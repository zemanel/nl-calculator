define(function () {

    var OPERATOR_ADD = "+",
        OPERATOR_SUB = "-",
        OPERATOR_MUL = "*",
        OPERATOR_DIV = "/",
        OPERATORS = [OPERATOR_ADD, OPERATOR_SUB, OPERATOR_MUL, OPERATOR_DIV],
        PRECEDENCE = {
            "+": 1,
            "-": 1,
            "*": 2,
            "/": 2,
        };

    // expr Parser 
    var parser = function(){};


    // pre-process expression to simplify algorithm
    parser.prototype.preProcess = function(expr) {
        console.group("preProcess");
        expr = expr.toLowerCase();

        // Trim whitespace
        expr = expr.replace(/^\s+|\s+$/gm, "");

        // replaces 2+ spaces with 1
        expr = expr.replace(/\s+/g, " ");

        // replace operator aliases with arithmetic ones
        expr = expr.replace(/(add)|(plus)/g, OPERATOR_ADD);
        expr = expr.replace(/(subtract)|(minus)|(less)/g, OPERATOR_SUB);
        expr = expr.replace(/(multiply-by)|(times)/g, OPERATOR_MUL);
        expr = expr.replace(/(divide-by)|(over)/g, OPERATOR_DIV);

        // replace integer aliases
        expr = expr.replace(/zero/g, '0');
        expr = expr.replace(/one/g, '1');
        expr = expr.replace(/two/g, '2');
        expr = expr.replace(/three/g, '3');
        expr = expr.replace(/four/g, '4');
        expr = expr.replace(/five/g, '5');
        expr = expr.replace(/six/g, '6');
        expr = expr.replace(/seven/g, '7');
        expr = expr.replace(/eight/g, '8');
        expr = expr.replace(/nine/g, '9');

        console.debug("Preprocessed expression as:", expr);

        console.groupEnd();

        return expr;
    };

    parser.prototype.isOperator = function(token) {
        return OPERATORS.indexOf(token) != -1;
    }

    parser.prototype.isNumeric = function(token) {
        return token >= '0' && token <= '9';
    }

    parser.prototype.compute = function(operator, a, b) {
        console.group("compute")
        var val;
        if (operator == OPERATOR_ADD) {
            val = a + b;
        } else if (operator == OPERATOR_SUB) {
            val = a - b;
        } else if (operator == OPERATOR_MUL) {
            val = a * b;
        } else if (operator == OPERATOR_DIV) {
            val = a / b;
        }
        console.debug("Computed", a, operator, b, "as", val);
        console.groupEnd()
        return val;
    }
    
    parser.prototype.parseExpression = function(expr) {
        console.group("parseExpression");

        var tokens = this.preProcess(expr).split(" "),
            operandStack = [],
            operatorStack = [],
            token,
            operator, num1, num2, rslt;

        console.debug(tokens);

        // while there are tokens remaining in the input string
        while (tokens.length) {

            // get the next token
            token = tokens.shift();
            console.debug("Current token", token)
            if (this.isNumeric(token)) {
                // if the token is a number, push it onto the operand stack
                operandStack.push(parseInt(token, 10));

            } else if (this.isOperator(token)) {
                // else if the token is an operator (+, -, *, /)    
                // while the priority of the new token is less than or equal 
                // to that of the top operator in the operator stack:
                while (PRECEDENCE[token] <= PRECEDENCE[operatorStack[operatorStack.length - 1]]) {
                    // pop an operator from the operator stack
                    operator = operatorStack.pop();
                    // pop two values from the operand stack
                    num2 = operandStack.pop();
                    num1 = operandStack.pop();
                    // apply the operator to the two values to compute a new value
                    rslt = this.compute(operator, num1, num2);
                    // put the new value onto the operand stack
                    operandStack.push(rslt);
                }

                // push the new token onto the operator stack
                operatorStack.push(token);
            }
            console.log("operatorStack:", operatorStack);
            console.log("operandStack:", operandStack);
        }

        console.debug("No more tokens");

        // pop operators till operator stack is not empty 
        while (operatorStack.length) {
            operator = operatorStack.pop();
            // pop top 2 operands and push op1 op op2 on the operand stack            
            num2 = operandStack.pop();
            num1 = operandStack.pop();
            rslt = this.compute(operator, num1, num2);
            // put the new value onto the operand stack
            operandStack.push(rslt);
        }
        
        console.groupEnd();
        return operandStack[0];
    };


    return parser;
});
