define(["app/NLCalculator"], function(NLCalculator){

    describe("NLCalculator", function() {

        var calc = new NLCalculator();

        describe("preProcess", function() {

            it("should preprocess expression 1", function() {
                expect(calc.preProcess("zero one two three four five six seven eight nine")).toEqual("0 1 2 3 4 5 6 7 8 9");
            });
            it("should preprocess expression 2", function() {
                expect(calc.preProcess("nine over eight plus four times two divide-by three")).toEqual("9 / 8 + 4 * 2 / 3");
            });
            it("should preprocess expression 3", function() {
                expect(calc.preProcess("nine minus three times seven")).toEqual("9 - 3 * 7");
            });
            it("should preprocess expression 4", function() {
                expect(calc.preProcess("seven over nine plus one")).toEqual("7 / 9 + 1");
            });
        });

        describe("compute", function() {

            it("should compute to 5", function() {
                expect(calc.compute("+", 2, 3)).toEqual(5);
            });
            it("should compute to -1", function() {
                expect(calc.compute("-", 2, 3)).toEqual(-1);
            });
            it("should compute to 6", function() {
                expect(calc.compute("*", 2, 3)).toEqual(6);
            });
            it("should compute to 0.6666666666666666", function() {
                expect(calc.compute("/", 2, 3)).toEqual(0.6666666666666666);
            });
        });

        describe("parseExpression", function() {
            it("should parse expression to 1.125", function() {
                expect(calc.parseExpression("nine over eight")).toEqual(1.125);
            });
            it("should parse expression to 3.7916666666666665", function() {
                expect(calc.parseExpression("nine over eight plus four times two divide-by three")).toEqual(3.7916666666666665);
            });
            it("should evaluate expression to 7", function() {
                expect(calc.parseExpression("one plus two times three")).toEqual(7);
            });
            it("should evaluate expression to -12", function() {
                expect(calc.parseExpression("nine minus three times seven")).toEqual(-12);
            });
            it("should evaluate expression 1.7777777777777777", function() {
                expect(calc.parseExpression("seven over nine plus one")).toEqual(1.7777777777777777);
            });
        });
    });
});
