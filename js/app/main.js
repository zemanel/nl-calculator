/**
  * Entrypoint script for processing user expressions.
  */
define(["app/NLCalculator"], function (NLCalculator) {
    var txtInput = document.getElementById("expression"),
        calculateBtn = document.getElementById("calculateBtn"),
        resultEl = document.getElementById("result"),
        calculator = new NLCalculator(),
        expr, rslt;

    calculateBtn.addEventListener('click', function() {
        console.group("app/main");
        console.log("Calculating expression:", txtInput.value);

        if (txtInput.value) {
            rslt = calculator.parseExpression(txtInput.value);
            resultEl.innerHTML = "<p> Result:" + rslt + "</p>";
        } else {
            resultEl.innerHTML = "<p>Valid expression not provided</p>";
        }

        console.groupEnd();
    });

});
