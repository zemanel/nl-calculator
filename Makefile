#
# 
#

RELEASE_DIR=release

all: clean build


build: clean
	r.js -o build.js


setup:
	npm install

clean:
	rm -rf $(RELEASE_DIR)/*
