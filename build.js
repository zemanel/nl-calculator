{
  "baseUrl": "js",
  "paths": {
    //"app": "js/app"
  },
  "dir": "release",
  "removeCombined": true,
  "optimize": "none",
  "fileExclusionRegExp": /^\.DS_Store/,
  "modules": [
    // component without additional libs
    {
      "name": "nl-calculator",  //module names are relative to baseUrl
      "create": true,
      // dependencies
      "include": [
        "app/main"
      ]
    }
  ] // end of modules
}
