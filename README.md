# About

A natural language calculator example. Supports whole numbers between zero and nine and basic arithmetic (add, subtract, divide and multiply), for example: "nine over eight plus four times two divide-by three minus one times seven".

The following operators (and aliases) are supported:

- Add (+) : add, plus 
- Subtract (-) : subtract, minus, less 
- Multiply (*): multiply-by, times 
- Divide (/): divide-by, over 


# References

- http://programmers.stackexchange.com/questions/254074/how-exactly-is-an-abstract-syntax-tree-created
- http://stackoverflow.com/questions/13421424/how-to-evaluate-an-infix-expression-in-just-one-scan-using-stacks#answer-16068554
- http://stackoverflow.com/questions/12122161/infix-expression-evaluation?rq=1
- https://github.com/volojs/create-template/
- https://en.wikipedia.org/wiki/Shunting-yard_algorithm
- http://www.cis.upenn.edu/~matuszek/cit594-2012/Pages/expression-evaluator.html
